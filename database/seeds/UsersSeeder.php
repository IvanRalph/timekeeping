<?php

use Illuminate\Database\Seeder;
use App\User as User;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $users = User::insert([
            [
                "user_id" => "10047",
                "employee_id" => 0,
                "name" => "Chris Nevalga",
                "email" => "chris.nevalga@solarphilippines.ph",
                "password" => bcrypt("password@1"),
                "initialized" => 1
            ],
            [
                "user_id" => "10048",
                "employee_id" => 1,
                "name" => "Ralph Vitto",
                "email" => "ralph.vitto@solarphilippines.ph",
                "password" => bcrypt("itsupport"),
                "initialized" => 1,
            ]
        ]);
    }
}
